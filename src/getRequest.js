function logResponse(responseCode) {
	console.log(responseCode);
}

function getRequest(fileName, onSuccess, onFailure = logResponse, modifyRequest = () => {}) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (request.readyState == XMLHttpRequest.DONE) {
			if (request.status == 200) {
				onSuccess(request.responseText);
			} else {
				onFailure(request.status);
			}
		}
	};
	request.open("GET", fileName, true);
	request.send();
}
