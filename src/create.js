function create(elementType) {
	let newElement = document.createElement(elementType);
	for (let i = 1; i < arguments.length; i++) {
		let currentArgument = arguments[i];		
		if (typeof(currentArgument) === 'string') {
			newElement.innerHTML += currentArgument;
		} else if (Array.isArray(currentArgument)) {
			for (let j = 0; j < arguments[i].length; j++) {
				if (typeof(arguments[i][j]) === 'string') {
					newElement.innerHTML += currentArgument[j];		
				} else {	
					newElement.appendChild(currentArgument[j]);
				}
			}
		} else if (currentArgument instanceof Element) {
			newElement.appendChild(currentArgument);
		} else if (typeof(currentArgument) === 'function') {
			currentArgument(newElement);
		} else {
			Object.getOwnPropertyNames(currentArgument).forEach(
				function (val, idx, array) {
					newElement.setAttribute(val, currentArgument[val]);
				}
			);
		}
	}
	return newElement;
}

for (let e of [
	"a","abbr","acronym","address","applet","area","article",
	"aside","audio","base","basefont","bdo","bgsound","blink",
	"blockquote","body","br","button","canvas","caption","center",
	"col","colgroup","command","comment","datalist","dd","del",
	"details","dir","div","dl","dt","embed","fieldset","figure","b",
	"big","i","small","tt","font","footer","form","frame","frameset",
	"head","header","hgroup","h1", "h2", "h3", "h4", "h5", "h6", "hr","html","isindex","iframe",
	"ilayer","img","input","ins","keygen","keygen","label","layer",
	"legend","li","link","map","mark","marquee","menu","meta",
	"meter","multicol","nav","nobr","noembed","noframes","noscript",
	"object","ol","optgroup","option","output","p","param","cite",
	"code","dfn","em","kbd","samp","strong","var","plaintext","pre",
	"progress","q","ruby","script","section","select","spacer",
	"span","s","strike","style","sub","sup","table","tbody","td",
	"textarea","tfoot","th","thead","time","title","tr","u","ul",
	"video","wbr","wbr","xmp"
]) {
	window[e] = function() {
		let args = [e].concat(Array.from(arguments));
		console.log(args);
		return create.apply(null, args);
	}
}

